# Linux Misc Configuration

Linux miscellaneous configuration (fizzy-compliant).

## Documentation

See [wiki](https://gitlab.com/fizzycfg/official-cfg/configs-linux-misc/wikis/home)

## Contributing

See [CONTRIBUTING](./CONTRIBUTING.md)

## License

See [LICENSE](./LICENSE)
